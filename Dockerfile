FROM node:10-alpine
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
ENV NODE_ENV=dev
EXPOSE 3001
CMD [ "npm", "start" ]
