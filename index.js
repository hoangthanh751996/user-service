const express = require('express')
const morgan = require('morgan')
const postman = require('postman');
const config = require('config');

// nats.io
const nats = require("./nats");

const app = express()
app.use(morgan('dev'))
app.get('/healthz', (req, res) => res.send('OKAY!'))
app.get('/user', (req, res) => res.json({id: "123", name: "Kha on lan 12345"}))
app.post('/user', (req, res) => {
    nats.publish('auth_service.user.created', JSON.stringify({name: "Kha on", email: "khaon@high.vn", id: 1}))
    res.send("user created")
})
app.get('/', (req, res) => res.send('Hello user-service'))

const port = process.env.PORT || config.get('PORT') || 3001
const host = process.env.HOST || config.get('HOST') || '0.0.0.0'

app.listen(port, (err) => {
    if (!err) console.log(`user-service is running on: ${host}:${port}`);
});
