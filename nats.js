const config = require("config");
// nats.io
const NATS_HOST = process.env.NATS_HOST || config.get('nats.host') || "localhost";
const NATS_PORT = process.env.NATS_PORT || config.get('nats.port') || 4222;

const nats = require("nats").connect(`nats://${NATS_HOST}:${NATS_PORT}`);

nats.on("error", function (e) {
    console.log("Error [" + nats.options.url + "]: " + e);
    process.exit();
});

nats.on("close", function () {
    console.log("Nats broker is closed");
    process.exit();
});

console.log("Connect to Nats broker successfully!");

module.exports = nats;
